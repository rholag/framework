//import * as internal from "stream";

export interface presentation {
    name: string;
    age: number;
    mail: string;
    adresse: string;
    num: string;
  }
export interface formation {
    formation: string;
    lieu: string;
    temps_formation: string;
    date: string;
}
export interface experiance {
    experiance: string;
    lieu: string;
    temps_experiance: string;
    date: string;
}
export interface hobbit {
    loisir: string;
}
