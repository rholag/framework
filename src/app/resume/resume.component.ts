import { Component, OnInit } from '@angular/core';
import { resume } from '../resumer-parcours';
@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss']
})
export class ResumeComponent implements OnInit {

  items = [
    {
      plan: "Diplomer d'une Licence 3 en MIAGE à la FAC AMU en économie gestion,"
    },
    {
      plan: "Actuellment en master 1 info tech à Campus Academy, en alternance en Data Analyst à la SNCF"
    },
    {
      plan: "je suis à la recherche d'un CDI dans le poste que j'occupe actuellement dans mon alternance."
    }
];

  constructor() { }

  ngOnInit(): void {
  }

}
