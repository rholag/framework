import { Component, OnInit } from '@angular/core';
import { hobbit, presentation } from '../cv';
import { formation } from '../cv';
import { experiance } from '../cv';
@Component({
  selector: 'app-cv-body',
  templateUrl: './cv-body.component.html',
  styleUrls: ['./cv-body.component.scss']
})
export class CvBodyComponent implements OnInit {
  pres: presentation = {
    name: 'Romain Lagarce',
    age: 26,
    mail: 'romain.lagarce@outlook.com',
    adresse: 'aix en provence',
    num: '07.86.XX.XX.XX',
  }
   items = [
        {
          formation: 'master 1 info-tech',
          lieu: 'Campus-academy',
          temps_formation: '2 ans',
          date: '09/2021',
        },
        {
          formation: 'Licence MIAGE',
          lieu: 'AMU',
          temps_formation: '3 ans',
          date: '2017 à 2020',
        }
    ];
    Booklist = [
      {
        experiance: 'data analyst',
        lieu: 'SNCF',
        temps_experiance: '1 an',
        date: '09/2021',
      },
      {
        experiance: 'Libraire',
        lieu: 'Reve de Manga',
        temps_experiance: 'travail a mi-temps',
        date: 'travail saisonnier',
      }
  ];
  hobbits = [
    {
      loisir: 'badminton',
    },
    {
      loisir: 'art appliquer',
    }
];

  constructor() { }

  ngOnInit(): void {
  }

}